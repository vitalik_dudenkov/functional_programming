const fs = require("fs");
const _ = require('lodash')
const data = require('./data')


function task1(){
    let colors = require('./colors.json');
    console.log(_(colors)
                .map(x => Object.keys(x)[0])
                .filter(x => x.length < 6)
                .sortBy()
                .value())
}

function task2(){
    let colors = require('./colors.json')
    console.log(_.
        sortBy(_
            .zip(_
                .map(_
                    .map(colors, x => Object.keys(x)), x => x[0]), _
                .map(_
                    .map(colors, x => Object.values(x)), x => x[0].slice(0, x[0].length - 1)))
            .map(arr => _.zipObject(['color', 'rgb'], arr)),
            a => a.color
        )
    )
}

function task3(){
    let users = require('./users.json')
    console.log(_
        .orderBy(_
            .zip(_
                .map(_.filter(users, x => x.address.geo.lat < 0), x => x.username), _
                .map(_.filter(users, x => x.address.geo.lat < 0), x => x.address.city))
            .map(arr => _.zipObject(['username', 'city'], arr)), ['city'], ['desc']
        )
    )
}

function task4(){
    let clients = require('./clients.json').clients
    console.log(_
        .orderBy(_
            .filter(clients, x => x.address.city === 'Кунгур'), ['gender', 'age', 'name'], ['asc', 'desc', 'asc'])
    )
}

function task5(){
    console.log(_
        .orderBy(_
            .zip(data.colors, _.map(data.argb, x => rgbToHex(x)))
            .map(arr => _.zipObject(['color', 'hex_name'], arr)), ['color'], ['asc'])
    )
}

function rgbToHex (arr) {
    var outParts = [
      arr[0].toString(16),
      arr[1].toString(16),
      arr[2].toString(16),
    ];
  
    outParts.forEach(function (part, i) {
      if (part.length === 1) {
        outParts[i] = '0' + part;
      }
    })
  
    return ('#' + outParts.join(''));
}

//task5()
//task4()
//task3()
//task2()
//task1()

