const fs = require('fs');
const csvjson = require('csvjson');
const mysql = require("mysql2");

function csv_to_json(nameFile) {
	let textCSV = fs.readFileSync(nameFile, 'utf-8');
	return csvjson.toObject(textCSV, { delimiter: ',' });
}

function get_connection() {
	return mysql.createConnection({
		host: "pgsha.ru",
		port: "35006",
		user: "soft0064",
		password: "q2Nf5Nw6",
		database: "soft0064_labrab06"    
	});
}

module.exports.csv_to_json = csv_to_json;
module.exports.getConn = get_connection;