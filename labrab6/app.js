const mysql = require("mysql2");
const express = require("express");
const ut = require('./utils');

const pool = mysql.createPool({
    host: "pgsha.ru",
    port: "35006",
    user: "soft0064",
    password: "q2Nf5Nw6",
    database: "soft0064_labrab06"    
});

const app = express();
const urlencodedParser = express.urlencoded({extended: false});
app.use('/css', express.static(__dirname + '/css'));
app.set("view engine", "hbs");


app.get("/", function(req, res) {
    let query = "SELECT * FROM movie";
    pool.query(query, function(err, data) {
        if (err) return console.log(err);
        res.render("index.hbs", {
            movie: data
        });
    });
});


app.get("/create", function(req, res) { 
    res.render("create.hbs");
});

app.post("/create", urlencodedParser, function (req, res) { // сохранить запись в БД
    if (!req.body) return res.sendStatus(400);
    const title = req.body.title;
    const director = req.body.director;
    const duration = req.body.duration;
    const rate = req.body.rate;
    let query = "INSERT INTO movie (title, director, duration, rate) VALUES (?,?,?,?)";
    let params = [title, director, duration, rate];
    pool.query(query, params, function(err, data) {
        if (err) return console.error(err);
        res.redirect("/");
    });
});


app.get("/edit/:id", function(req, res) {
    const id = req.params.id;
    pool.query("SELECT * FROM movie WHERE id=?", [id], function(err, data) {
        if (err) return console.error(err);
        res.render("edit.hbs", {
            movie: data[0]
        });
    });
});

app.post("/edit", urlencodedParser, function (req, res) {
    if (!req.body) return res.sendStatus(400);
    const id = req.body.id;
    const title = req.body.title;
    const director = req.body.director;
    const duration = req.body.duration;
    const rate = req.body.rate;
    let query = "UPDATE movie SET title=?, director=?, duration=?, rate=? WHERE id=?";
    let params = [title, director, duration, rate, id];
    pool.query(query, params, function(err, data) {
        if (err) return console.error(err);
        res.redirect("/");
    });
});


app.post("/delete/:id", function(req, res) {
    const id = req.params.id;
    pool.query("DELETE FROM movie WHERE id=?", [id], function(err, data) {
        if (err) return console.log(err);
        res.redirect("/");
    });
});


app.get("/sort/:field.:direct", function(req, res) {
    const field = req.params.field;
    const direct = req.params.direct;
    let query = "SELECT * FROM movie ORDER BY " + field + " " + direct;
    pool.query(query, function(err, data) {
        if (err) return console.log(err);
        res.render("index.hbs", {
            movie: data
        });
    });
});

app.get("/restoreDefault", function(req, res){
    let truncQuery = "TRUNCATE movie";
    let insQuery = "INSERT INTO movie (title, director, duration, rate) VALUES ?";

    const conn = ut.getConn();
    let array = ut.csv_to_json('./data.csv');
    let rows = array.map(item => Object.values(item));

    conn.promise()
    .query(truncQuery)
    .then(() => { 
        conn.promise()
            .query(insQuery, [rows])
            .catch((err) => console.error('ins -> ', err));
    })
    .then(() =>{
        conn.promise()
            .query("SELECT * FROM movie")
            .then(([data]) =>{
                res.render("index.hbs", {
                    movie: data
                });
            })
            .then(conn.end())
            .catch((err) => console.error('select ->', err))
    })    
    .catch((err) => console.error('trunc ->', err));
});

app.listen(3000, function() {
    console.log("смотрим работу через браузер - http://localhost:3000/");
    let isWin = process.platform === "win32";
    let hotKeys = isWin? "Ctrl+C": "Ctrl+D"; // Windows / Linux
    console.log(`остановить сервер - ${hotKeys}`);
});