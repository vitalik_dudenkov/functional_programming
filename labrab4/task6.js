const ut = require('./utils');

let query = "SELECT * FROM `patient` WHERE name= ?";

let params = ['laura abc']

const conn = ut.get_conn();

conn.promise()
    .query(query, params)
    .then(([rows]) => console.table(rows))
    .catch((err) => console.error(err))
    .then(conn.end());

// Время выполнения запроса без индекса: 0.0117 сек.
// время выполнения запроса с индексом: 0.0015 сек.