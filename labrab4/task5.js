const ut = require('./utils');

let query = "SELECT COUNT(*) FROM `patient`";

const conn = ut.get_conn();

conn.promise()
    .query(query)
    .then(([rows]) => console.table(rows))
    .catch((err) => console.error(err))
    .then(conn.end());