const ut = require('./utils');

let query_create = "CREATE TABLE `patient` ( \
    `id` INT NOT NULL AUTO_INCREMENT , \
    `day` DATE NOT NULL , \
    `city` VARCHAR(255) NOT NULL , \
    `name` VARCHAR(255) NOT NULL , \
    `count` INT NOT NULL, \
    PRIMARY KEY (`id`))";

const conn = ut.get_conn();

conn.promise()
    .query(query_create)
    .then(() => console.log('table created'))
    .catch((err) => console.error(err))
    .then(conn.end());
