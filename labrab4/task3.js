const ut = require('./utils');

let query_create = "TRUNCATE `patient`";

const conn = ut.get_conn();

conn.promise()
    .query(query_create)
    .then(() => console.log('table truncated'))
    .catch((err) => console.error(err))
    .then(conn.end());