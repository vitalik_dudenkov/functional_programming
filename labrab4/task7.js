const ut = require('./utils');

let query = "SELECT id, DATE_FORMAT(day, '%Y.%m.%d'), city, name, count \
            FROM `patient` \
            ORDER BY count DESC \
            LIMIT 20";

const conn = ut.get_conn();

conn.promise()
    .query(query)
    .then(([rows]) => console.table(rows))
    .catch((err) => console.error(err))
    .then(conn.end());