const ut = require('./utils');

let query = "SELECT day, city, name, count \
            FROM patient \
            WHERE name = ( \
                        SELECT name \
                        FROM patient \
                        ORDER BY count DESC \
                        LIMIT 1) \
            ORDER BY day";

const conn = ut.get_conn();

conn.promise()
    .query(query)
    .then(([rows]) => console.table(rows))
    .catch((err) => console.error(err))
    .then(conn.end());